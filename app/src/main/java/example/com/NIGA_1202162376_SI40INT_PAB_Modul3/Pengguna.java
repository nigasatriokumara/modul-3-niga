package example.com.NIGA_1202162376_SI40INT_PAB_Modul3;

public class Pengguna {

    private String Nama;
    private String Pekerjaan ;
    private String JenisKelamin ;
    private int Thumbnail ;

    public Pengguna() {
    }

    public Pengguna(String nama, String pekerjaan, String jenisKelamin, int thumbnail) {
        Nama = nama;
        Pekerjaan = pekerjaan;
        JenisKelamin = jenisKelamin;
        Thumbnail = thumbnail;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getPekerjaan() {
        return Pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        Pekerjaan = pekerjaan;
    }

    public String getJenisKelamin() {
        return JenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        JenisKelamin = jenisKelamin;
    }

    public int getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        Thumbnail = thumbnail;
    }




}

